# Voirie


**Les hypothèses financières** pour les estimations de coûts de chaque option liée à la voirie sont les suivantes :
- Environ _10 km_ de voirie dans le nouveau quartier
- Une voirie uniquement goudronnée demande un investissement de _320 000 €/km_
- Une voirie partiellement goudronnée, partiellement avec des petits pavés demande un investissement de _350 000 €/km_
- Une voirie uniquement avec des petits pavés demande un investissement de _450 000 €/km_


![ALT Travaux de voirie](voirie.jpeg "Travaux de voirie")