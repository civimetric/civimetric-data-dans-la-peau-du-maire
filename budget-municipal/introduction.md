### Imaginez...

Nous vous proposons d'**imaginer que vous êtes un habitant qui va emménager dans un nouveau quartier de 80 hectares**. Les élus locaux sollicitent votre avis sur le réaménagement de ce quartier. Votre avis concernera l'aménagement des structures proches de votre futur logement et accessibles à pied. 

Ce quartier est situé dans une zone urbaine au sein d'une ville qui dispose déjà d'infrastructures variées (piscine, salle de spectacle, patinoire...) mais situées hors du nouveau quartier.

Ce questionnaire devrait vous prendre **5 minutes au total**. Bonne séance !